#import "GPUImageTwoInputFilter.h"

@interface GPUImageAddBlendFilter : GPUImageTwoInputFilter
{
    GLint mixUniform;
}


@property(readwrite, nonatomic) CGFloat mix;

@end
